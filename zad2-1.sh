#!/bin/bash

echo
basename $1   | tee -a  $2
stat -c "%U" $1    | tee -a  $2

katalog=$1

echo "Liczba wierszy w katalogu"  | tee -a $2
wc -l $katalog/* | tee -a $2
echo
echo "Liczba znakow w katalogu"  | tee -a $2
wc -m $katalog/*  | tee -a $2