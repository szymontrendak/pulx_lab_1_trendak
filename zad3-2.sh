#!/bin/sh
echo "Podaj Haslo:";
read pass;
echo "Twoje haslo to: $pass";
echo "Podaj co chcesz zrobic:(a/b/c/*\na)Liczba duzych liter\nb)Liczba malych liter\nc)Liczba cyfr\n*)Liczba innych znakow\nWYBIERZ:   ";
read select;
case "$select" in
    a)echo -n "Liczba duzych liter: ";
    lm=$(echo -n $pass | grep -o [A-Z] | tr -d "\n" | wc -m);
    echo "$lm";;
    b)echo -n "Liczba malych liter: ";
    ld=$(echo $pass | grep -o [a-z] | tr -d "\n" | wc -m);
    echo "$ld";;
    c)echo -n "Liczba cyfr: ";
    lc=$(echo $pass | grep -o [0-9] | tr -d "\n" | wc -m);
    echo "$lc";;
    *)val=$(($(echo -n $pass | wc -m) - $lm - $ld - $lc));
    echo "Liczba innych znakow: $val";;
esac
exit 0